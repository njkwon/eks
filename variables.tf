variable "region" {
  default = "ap-northeast-2"
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "883545701064"
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::883545701064:role/njkwon"
      username = "njkwon"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::883545701064:user/vardin"
      username = "vardin"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::883545701064:user/yjkim"
      username = "yjkim"
      groups   = ["system:masters"]
    },
  ]
}
