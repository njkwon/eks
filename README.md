목표 : Terraform 을 이용해서 eks 의 모든 기능을 구성해본다. 

Requirement 

1. AWS CLI
```
$brew install awscli
$aws configure 
aws console 에서 access key, security key 를 받아온다. 

```

2. kubectl install on mac
```
brew install kubectl 
```

Terraform 시작

참고 : https://learn.hashicorp.com/tutorials/terraform/install-cli 

1. install terraform on mac os 

```
$brew install hashicorp/tap/terraform
$ terraform version
   
```

2. install helm on mac os 
```
$brew install helm
$ helm version 
version.BuildInfo{Version:"v3.2.4"
```
helm 이 3.0 이상이면 tiller를 따로 구성하지 안아도 된다. 

3. git 설치 
```
brew install git
```

4. source 코드를 가져온다. 
```
 git clone https://njkwon@bitbucket.org/njkwon/eks.git

```

5. terraform 실행하기 
```
$cd eks 
$vi main.tf

56 line cluster_name="[클러스터이름]"
56라인의 클러스터 명을 수정해준다.
저장 

$terraform init
$terraform plan
$terraform apply
yes

몇분후에 클러스터와 nodegroup이 생성된다. 


```

1. 쿠버 config에 cluster 등록

aws eks --region ap-northeast-2 update-kubeconfig --name [클러스터이름]

2. kubernates 에 tiller 서버를 설정한다. 
```
$  cat rbac-config.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system


helm 3.0 이상일경우에는 tiller가 필요 없다. 보안때문에 빼버림 


kubectl apply -f rbac-config.yaml

helm init --service-account tiller

helm init --service-account tiller --history-max 200
```
3. prometheus 설치
```
kubectl create namespace prometheus

helm install prometheus stable/prometheus   --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2",server.service.type=LoadBalancer

aws에서 필요한 helm chart report 추가 참고사이트
 https://github.com/aws/aws-node-termination-handler

helm chart 추가 
helm repo add eks https://aws.github.io/eks-charts
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update


helm install stable/mariadb

repo 등록 
nginx 설치
helm install mywebserver   bitnami/nginx


label 확인을 위해서 다음과 같이한다. 
kubectl apply -f https://raw.githubusercontent.com/openshift-evangelists/kbe/main/specs/labels/pod.yaml


helm repo list
```
